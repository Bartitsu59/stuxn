STuxn
-----

This is an implementation for the Atari ST of the uxn/varvara virtual machine.
It is writtent in Pure Pascal which is mostly compatible with Turbo Pascal 7.0 DOS, but it uses ST hardware registers.
Sprites are **NOT** drawn pixel by pixel but using bit block transfers, so the speed is reasonably good for 8Mhz.

So far only a few feature are partialy implemented :
- console
- video
- core

2BPP Sprites are not implemented yet.

![Sprites shown in ST medium resolution](sprites.gif?raw=true "Sprites shown in ST medium resolution")
